const env = require('../lib/env');
const path = require('path');
const http = require('http');
const express = require('express');
const fallback = require('express-history-api-fallback');

const { BUILD_PATH, PORT } = env;

const app = express();
app.set('port', PORT);

const __dist = path.resolve(__dirname, '..', BUILD_PATH);
const __root = path.join(__dist, 'index.html');

const server = http.Server(app);

app.use(express.static(__dist));
app.use(fallback(__root));

server.listen(app.get('port'), () => {
  console.info('Server started at ' + PORT);
});