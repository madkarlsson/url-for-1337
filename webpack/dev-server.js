const defaultConfig = {
  contentBase: '.',
  historyApiFallback: {
    index: 'index.html'
  },
  hot: true,
  inline: true,
  host: '127.0.0.1',
  port: 8081
};

module.exports = function getDevServerConfig(env) {
  const contentBase = env.BUILD_PATH || defaultConfig.contentBase;

  return Object.assign({}, defaultConfig, {
    contentBase,
    host: env.DEV_SERVER_URL || defaultConfig.host,
    port: env.DEV_SERVER_PORT || defaultConfig.port
  });
};
