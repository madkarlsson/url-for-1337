const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const RobotstxtPlugin = require('robotstxt-webpack-plugin').default;

module.exports = function getPlugins(env) {
  // Default plugins
  let plugins = [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `"${env.NODE_ENV}"`,
        APP_TITLE: `"${env.APP_TITLE}"`,
        CLIENT_NAME: `"${env.CLIENT_NAME}"`,
      }
    }),
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: env.APP_TITLE,
      template: path.join(env.SRC_PATH, 'index.html'),
      filename: env.OFFLINE && env.CLIENT_NAME ? `${env.CLIENT_NAME}.html` : 'index.html',
      appMountId: 'app',
      hash: !env.OFFLINE,
      inject: !env.OFFLINE,
      inlineJs: env.OFFLINE
    })
  ];

  // Depending on if we are in production or not, we want to set up the build
  // differently
  if (env.isProduction) {
    plugins = plugins.concat([
      new CleanWebpackPlugin(env.BUILD_PATH, {
        root: path.resolve(env.BUILD_PATH, '..'),
      }),
      new UglifyJSPlugin({
        parallel: true
      })
    ]);

    // Offline build does not need a few things
    if (!env.OFFLINE) {
      plugins = plugins.concat([
        new RobotstxtPlugin({
          policy: [{
            userAgent: '*',
            disallow: '/',
          }]
        }),
        new FaviconsWebpackPlugin({
          logo: path.join(env.SRC_PATH, 'assets', 'favicon.png'),
          title: env.APP_TITLE ? `${env.APP_TITLE}` : undefined,
          icons: {
            android: true,
            appleIcon: true,
            appleStartup: true,
            coast: false,
            favicons: true,
            firefox: true,
            opengraph: true,
            twitter: true,
            yandex: false,
            windows: true
          }
        }),
      ]);
    }
  }
  else {
    plugins = plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
    ]);
  }

  return plugins;
};

