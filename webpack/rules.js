/**
 * Rules and loaders for webpack.
 */

const defaultRules = [
  {
    test: /\.vue$/,
    use: [
      "vue-loader",
      {
        loader: "vue-svg-inline-loader",
        options: { /* ... */ }
      },
      // ...
    ],
  }, {
    test: /\.js?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
  },
  {
    test: /\.css$/,
    use: [
      'vue-style-loader',
      'css-loader'
    ]
  },
  {
    test: /\.scss$/,
    use: [
      'vue-style-loader',
      'css-loader',
      'sass-loader',
    ]
  },
  {
    test: /\.svg$/,
    use: 'vue-svg-inline-loader'
  }
];

/**
 * Gets the URL-loader to use in build.
 *
 * Depending on if we are doing an offline-build or not we inline all assets
 * or not.
 *
 * @param {Object} env Environment object, defaults to current process object.
 * @return {Object} Loader object to use in webpack build.
 */
function getUrlLoader(env = process.env) {
  const options = {
    name: '[name].[ext]?[hash]',
    limit: 1
  };
  if (env.OFFLINE === true || env.OFFLINE === 'true') {
    options.limit = undefined;
  }
  return {
    test: /\.(png|jpe?g|gif|svg|mp4|m4v|eot|ttf)$/,
    exclude: /svg-sprite/,
    use: {
      loader: 'url-loader',
      options,
    }
  };
}

module.exports = function getRules(env) {
  return [].concat(defaultRules, [getUrlLoader(env)]);
};

