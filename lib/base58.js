// Methods taken and modified from https://coligo.io/create-url-shortener-with-node-express-mongo/

const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
const base = characters.length;

export function encode(value) {
  let encoded = '';
  while (value) {
    var remainder = value % base;
    value = Math.floor(value / base);
    encoded = characters[remainder].toString() + encoded;
  }
  return encoded;
}

export function decode(value) {
  let decoded = 0;
  while (value) {
    var index = characters.indexOf(value[0]);
    var power = value.length - 1;
    decoded += index * (Math.pow(base, power));
    value = value.substring(1);
  }
  return parseInt(decoded, 10);
}