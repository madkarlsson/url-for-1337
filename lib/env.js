/**
 * File that returns a version of `process.env` with some modified
 * values for use in a project.
 *
 * Processes configuration from dotenv and if available a "env" property
 * in `package.json`. Content in `process.env` and `.env`-file takes precedence
 * over values in `package.json`, which functions as a defaults-template.
 */

require('dotenv').config();
const path = require('path');
const __root = path.resolve(__dirname, '..');
const pkg = require(path.join(__root, 'package.json'));
const env = Object.assign(pkg.env || {}, process.env);

const { NODE_ENV, OFFLINE, BUILD_PATH, SRC_PATH } = env;
env.OFFLINE = OFFLINE === 'true';
env.isProduction = NODE_ENV === 'production';
env._SRC_PATH = SRC_PATH;
env.SRC_PATH = path.resolve(__root, SRC_PATH);
env._BUILD_PATH = BUILD_PATH;
env.BUILD_PATH = path.resolve(__root, BUILD_PATH);

module.exports = env;
