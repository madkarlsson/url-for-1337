import { expect } from 'chai';
import { encode, decode } from './base58';


describe('base58', () => {

  describe('encoding', () => {
    it('encodes a unique value based on input', () => {
      const sequence = 1000;
      const encoded = encode(sequence);
      const encoded2 = encode(sequence + 1);
      expect(encoded).not.to.equal(encoded2);
    });

    it('decodes value', () => {
      const decoded = decode('QY');
      expect(decoded).to.equal(1000);
    });
  });
});