const env = require('./lib/env');
const getDevServerConfig = require('./webpack/dev-server');
const getRules = require('./webpack/rules');
const getPlugins = require('./webpack/plugins');

const {
  BUILD_PATH,
  SRC_PATH,
  CLIENT_NAME,
  isProduction,
} = env;

module.exports = {
  devtool: !isProduction ? 'eval-source-map' : undefined,
  mode: isProduction ? 'production' : 'development',
  entry: {
    [CLIENT_NAME]: SRC_PATH,
  },
  output: {
    path: BUILD_PATH,
    filename: '[name].js',
    publicPath: '/'
  },
  module: {
    rules: getRules(env)
  },
  resolve: {
    extensions: ['.js', '.json', '.vue'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': SRC_PATH
    },

  },
  plugins: getPlugins(env),
  devServer: getDevServerConfig(env)
};