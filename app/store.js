import Vue from 'vue';
import Vuex from 'vuex';
import { encode, decode } from '../lib/base58';

Vue.use(Vuex);

export const state = {
  urls: [],
  urlSequenceId: 1000000000,
  persistent: false,
  ready: false,
};

function createUrl(url, id) {
  return {
    url,
    id,
    shortenedUrl: encode(id),
  };
}

export const mutations = {
  restore(state, fromState) {
    state.urls = fromState.urls;
    state.urlSequenceId = fromState.urlSequenceId;
  },
  persistance(state, isPersistent = true) {
    state.persistent = isPersistent;
  },
  makeReady(state) {
    state.ready = true;
  },
  setUrl (state, url) {
    const newUrl = createUrl(url, state.urlSequenceId);
    state.urlSequenceId++;
    state.urls.unshift(newUrl);
    if (state.urls.length > 10) {
      state.urls.pop();
    }
  },
  remove(state, urlToRemove) {
    state.urls = state.urls.filter(url => url.id !== urlToRemove);
  },
  removeAll(state) {
    state.urls = [];
  },
};

export const actions = {
  save({ commit, dispatch }, url) {
    commit('setUrl', url);
    dispatch('saveState');
  },
  saveState({ state }) {
    const stateToSave = {
      urls: state.urls,
      urlSequenceId: state.urlSequenceId
    };
    window.localStorage.setItem(process.env.CLIENT_NAME, JSON.stringify(stateToSave));
  },
  removeAndSave({ commit, dispatch }, url) {
    commit('remove', url);
    dispatch('saveState');
  },
  removeAllAndSave({ commit, dispatch }) {
    commit('removeAll');
    dispatch('saveState');
  },
  initiate({ commit }) {
    if ('localStorage' in window) {
      commit('persistance');
      const oldState = window.localStorage.getItem(process.env.CLIENT_NAME);
      if (oldState) {
        commit('restore', JSON.parse(oldState));
      }
    }
    commit('makeReady');
  },
};

export const getters = {
  url: state => {
    if (state.urls.length > 0) {
      return state.urls[0].url;
    }
    return null;
  },
  findUrl: state => urlKey => {
    if (state.urls.length > 0 && urlKey) {
      const decoded = decode(urlKey);
      const selectedUrl = state.urls.find(url => url.id === decoded);

      if (selectedUrl) {
        return selectedUrl.url;
      }
    }
    return null;
  },
  shortenedUrl: state => {
    if (state.urls.length > 0) {
      return state.urls[0].shortenedUrl;
    }
    return '';
  },
};

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
});