// Avoid registering polyfill globally if already registered
if (!global._babelPolyfill) {
  require('babel-polyfill');
}
import Vue from 'vue';
import VueClipboard from 'vue-clipboard2';
import store from '@/store';
import AppMain from '@/components/app-main';

Vue.use(VueClipboard);
Vue.component(AppMain.name, AppMain);

new Vue({
  store,
  render: h => h(AppMain),
}).$mount(document.getElementById('app'));
