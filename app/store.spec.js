import { expect } from 'chai';
import {
  mutations,
  state as storeState,
  getters,
} from './store';

const exampleUrl = 'http://example.com';
const startSequenceId = 1000;

describe('store', () => {
  let state;
  beforeEach(() => {
    state = { ...storeState };
  });

  describe('mutations', () => {
    it('setUrl', () => {
      state = {
        ...state,
        urlSequenceId: startSequenceId, // setting explicitally to ensure test results in case default changes
      };
      mutations.setUrl(state, exampleUrl);
      expect(state.urls.length).to.equal(1);
      expect(state.urlSequenceId).to.equal(startSequenceId + 1);
      expect(state.urls[0].url).to.equal(exampleUrl);
      expect(state.urls[0].shortenedUrl).to.equal('QY');

      mutations.setUrl(state, exampleUrl);
      expect(state.urls.length).to.equal(2);
      expect(state.urlSequenceId).to.equal(startSequenceId + 2);
      expect(state.urls[0].url).to.equal(exampleUrl);
      expect(state.urls[0].shortenedUrl).to.equal('QZ');

      mutations.setUrl(state, exampleUrl);
      expect(state.urls.length).to.equal(3);
      expect(state.urlSequenceId).to.equal(startSequenceId + 3);
      expect(state.urls[0].url).to.equal(exampleUrl);
      expect(state.urls[0].shortenedUrl).to.equal('Qa');

      mutations.setUrl(state, exampleUrl);
      expect(state.urls.length).to.equal(4);
      expect(state.urlSequenceId).to.equal(startSequenceId + 4);
      expect(state.urls[0].url).to.equal(exampleUrl);
      expect(state.urls[0].shortenedUrl).to.equal('Qb');
    });

    it('restore should be able to handle old state', () => {
      const urlSequenceId = startSequenceId + 2;
      const mockOldState = {
        ...state,
        urlSequenceId,
        urls: [
          { url: exampleUrl },
          { url: exampleUrl },
        ]
      };

      mutations.restore(state, mockOldState);
      expect(state.urls.length).to.equal(2);
      expect(state.urlSequenceId).to.equal(urlSequenceId);
      expect(state.urls[0].url).to.equal(exampleUrl);
      expect(state.urls[1].url).to.equal(exampleUrl);
    });

    it('makeReady', () => {
      mutations.makeReady(state);
      expect(state.ready).to.equal(true);
    });

    it('persistance should be able to be enabled/disabled', () => {
      mutations.persistance(state, true);
      expect(state.persistent).to.equal(true);

      mutations.persistance(state, false);
      expect(state.persistent).to.equal(false);
    });

    it('remove should be able to remove item in urls', () => {
      state = {
        ...state,
        urlSequenceId: startSequenceId + 2,
        urls: [
          { url: exampleUrl, id: startSequenceId, shortenedUrl: 'QY' },
          { url: exampleUrl, id: startSequenceId + 1, shortenedUrl: 'QZ' },
        ]
      };

      mutations.remove(state, startSequenceId + 1);
      expect(state.urls.length).to.equal(1);
      expect(state.urls[0].shortenedUrl).to.equal('QY');
    });

    it('remove should not break if url does not exist', () => {
      state = {
        ...state,
        urlSequenceId: startSequenceId + 2,
        urls: [
          { url: exampleUrl, id: startSequenceId, shortenedUrl: 'QY' },
          { url: exampleUrl, id: startSequenceId + 1, shortenedUrl: 'QZ' },
        ]
      };

      mutations.remove(state, 'nonexisting');
      expect(state.urls.length).to.equal(2);
    });

    it('remove all should clear urls', () => {
      state = {
        ...state,
        urlSequenceId: startSequenceId + 2,
        urls: [
          { url: exampleUrl, id: startSequenceId, shortenedUrl: 'QY' },
          { url: exampleUrl, id: startSequenceId + 1, shortenedUrl: 'QZ' },
        ]
      };

      mutations.removeAll(state);
      expect(state.urls.length).to.equal(0);
    });
  });

  describe('getters', () => {
    beforeEach(() => {
      state = {
        ...state,
        urlSequenceId: startSequenceId + 2,
        urls: [
          { url: exampleUrl, id: startSequenceId, shortenedUrl: 'QY' },
          { url: exampleUrl, id: startSequenceId + 1, shortenedUrl: 'QZ' },
        ]
      };
    });

    it('findUrl should find via ID', () => {
      // mock getter
      const urlToGet = 'QZ';
      expect(getters.findUrl(state)(urlToGet)).to.equal(exampleUrl);
    });

    it('findUrl should return null ID key does not exist ', () => {
      // mock getter
      const urlToGet = 'xyz';
      expect(getters.findUrl(state)(urlToGet)).to.equal(null);
    });
  });
});
