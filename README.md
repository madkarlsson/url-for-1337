# Worksample


## Requirements

Builds only tested with:
* NodeJS >= 10.4.0
* NPM >= 6.1.0

Tested with evergreen-browsers.


## Quick start

```
npm install
```

```
npm start
```

Go to http://localhost:8080 in favorite browser


## Notes about solution

Happy with the result really, would have done a few things differently in the
end. Size of the end result surprised me a little but is about 80kb gzipped
and ~30kb of those are polyfills.

App uses:
* VueJS + Vuex for UI and front end state management
* Webpack (babel) + webpack-dev-server for build and development
* ExpressJS to serve app in production (without the dev server)

About 8 hours effective time + a little research on shortening URLs. I
challenged myself to limit myself to 8 hours while broaching as many areas
as possible.

The base of the application is just a simple vuejs boiler I use as a simple base
for prototypes.

### URL shortening

Never done this before so read up a little on it. Seems like most solutions
base their shortening on sequential IDs that gets encoded with a
non-cryptographic function (so it allows decoding and lookup based on that).

My first idea was just to use base64 but it dawned on me that it can create
invalid URLs due to the characters so I based it on base58 instead (taken from
an article based on URL-shortening). worked good enough. It wasn't until
later that I realized that I misssed the requirement of the shortened URL
being 6 characters. Increasing the length of the original sequence ID creates
these correct length (and will do so for billions of URLs) so I deemed that good
enough for this solution.

If this would have been built as a production application I would have likely
leveraged the server and a database. It seemed a little overkill and I wanted
to create a solution that could be run offline, so all logic is on the
client-side.

There are a few flaws in this solution if it were to scale this solution and I
likely would have looked into other hashing functions and ways to handle this if
I were to redo it.

### State and storage

For the scale of the application, state management could actually have been made
through the components only (not using the store). Splitting it up increases
readability and structure though. The store and it's mutations and actions are
also more easily testable (functionally).

As persistance we use local storage. For larger scale data and calculations
I would have picked IndexedDB due to it's asynchronosity.


## Development

```
npm install
```

Use `npm run dev` for regular development. Check `package.json` for valid
development-commands.

> Note that for redirection to work in development you need to run `npm run build` first as the development server requires a proper index.html to be created.


## Building

All builds are run via `npm run build` (`npm start` also creates a build).
Configurations are made via environment-variables. All builds come in two
flavours, production or development. Set the environment variable `NODE_ENV` to
`production` to enable production mode.


Default environment variables that are important for the build are inherited
from `package.json` and if you need to change any variables it could be done
there, however if you consequentially need to change the variables locally
you can use a `.env`-file if you want, or set the variable before running the
command.

Ex on mac/linux:
```
NODE_ENV=production npm run build
```

If you are not familiar with `.env`-files, see
[the dotenv-package](https://www.npmjs.com/package/dotenv)


### Creating an offline build

Creating an offline build inlines the code into one html-file.
_Note that a JS file is also created but it's not necessary to use_. Set the
environment-variable `OFFLINE` to `true` before building.

The HTML-file created takes the name of the environment variable `CLIENT_NAME`
if set.

Note that persistance of URLs does not work due to that local storage is not
available unless you start browsers with special flags.

